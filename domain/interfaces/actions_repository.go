package main

type ResultSet []map[string]interface{}

type IActionsRepository interface {
	GetActions(query string) ResultSet
}
