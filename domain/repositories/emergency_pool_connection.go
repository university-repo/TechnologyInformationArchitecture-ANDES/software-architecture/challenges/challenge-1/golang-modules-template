package main

type EmergencyPoolConnection struct {
	AbstractPoolConnection
}

func (epc *EmergencyPoolConnection) Connect() (DatabaseConnection, error) {
	return nil, nil
}

func (epc *EmergencyPoolConnection) Disconnect(conn DatabaseConnection) error {
	return nil
}

func (epc *EmergencyPoolConnection) Read(query string) ResultSet {
	return ResultSet{}
}

func (epc *EmergencyPoolConnection) GetEmergencyActions() ResultSet {
	return ResultSet{}
}