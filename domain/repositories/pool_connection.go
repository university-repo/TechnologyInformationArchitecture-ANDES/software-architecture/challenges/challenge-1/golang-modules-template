package main

type IPoolConnection interface {
	Connect() (DatabaseConnection, error)
	Disconnect(DatabaseConnection) error
	Read(query string) ResultSet
}

type DatabaseConnection interface{}

type AbstractPoolConnection struct {
	IPoolConnection
}
